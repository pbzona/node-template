const express = require('express');
const router = express.Router();

// Controllers and handlers
const exampleController = require('../controllers/exampleController');
const { catchErrors } = require('../handlers/errorHandlers');

// Routes
router.get('/', catchErrors(exampleController.homePage));

module.exports = router;