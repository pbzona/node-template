# Node Template

This project is a template for a Node app using an MVC framework.

## Structure

The project is organized into the following folders:

- **controllers** - Delivers templates after taking a set of actions
- **handlers** - Handle exceptions, errors, and other request cases
- **models** - Data models for the database and structured template
- **public** - Static assets
- **routes** - Sends requests to the proper controllers
- **views** - Template files for rendering

And the following root files:

- **app.js** - Defines the application options
- **helpers.js** - Extra functions and settings to be reused throughout the app
- **start.js** - Runs the application
